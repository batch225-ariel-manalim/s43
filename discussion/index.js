let posts = []; //storage
let count = 1; //id

// add post data.

const post = document.querySelector('#form-add-post');
post.addEventListener('submit', (e) => {

    e.preventDefault();

    posts.push({
        id: count,
        title: document.querySelector('#txt-title').value,
        body: document.querySelector('#txt-body').value
    });

    count++;
    console.log(posts);
    showPosts(posts)
    alert('Successfully Added');
});

// Show posts
const showPosts = (posts) => {
    let postEntries = '';
    posts.forEach((post) => {

        postEntries += `
            <div id='post-${post.id}'>
                <h3 id='post-title-${post.id}'>${post.title}</h3>
                <p id='post-body-${post.id}'>${post.body}</p>
                <button onclick="editPost('${post.id}')">Edit</button>
			    <button onclick="deletePost('${post.id}')">Delete</button>
            </div>
        `
    });
    document.querySelector('#div-post-entries').innerHTML = postEntries;
};

// Edit post
const editPost = (id) => {
    let title = document.querySelector(`#post-title-${id}`).innerHTML;
    let body = document.querySelector(`#post-body-${id}`).innerHTML;

    document.querySelector('#txt-edit-id').value = id;
    document.querySelector('#txt-edit-title').value = title;
    document.querySelector('#txt-edit-body').value = body;
};

// update post
const update = document.querySelector('#form-edit-post');
update.addEventListener('submit', (e) => {
    e.preventDefault();
    for (let i = 0; i < posts.length; i++) {
        // The value posts[i].id is a number while document.querySelector('#txt-edit-id').value is a String.
        // Therefore, it is necessary to convert the Number to a String first.


        if (posts[i].id.toString() === document.querySelector('#txt-edit-id').value) {
            posts[i].title = document.querySelector('#txt-edit-title').value;
            posts[i].body = document.querySelector('#txt-edit-body').value;

            showPosts(posts);
        }
        alert('Successfully updated.');

        break;

    }
})

// Activity
// delete post
const deletePost = (id) => {
    for (const post of posts) {
        if (post.id.toString() == id) {
            const index = posts.indexOf(post);
            posts.splice(index, 1);
            break;
        }
    }
    showPosts(posts);
    alert('Successfully deleted!');
};